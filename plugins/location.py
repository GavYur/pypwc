# -*- coding: utf-8 -*-

# Python Perfect World Client,
# Location plugin
# Handle location info
# (c) GavYur, 2o13

import packets

def location_info_handler(client, locationinfo):
    client.char.loc.set_dungeon(locationinfo.locid)

def player_position_handler(client, playerpos):
    client.char.loc.set_coords(playerpos.X, playerpos.Y, playerpos.Z)

on_packet = {packets.sc2c.LocationInfo: [location_info_handler],
             packets.sc2c.PlayerPosition: [player_position_handler]}