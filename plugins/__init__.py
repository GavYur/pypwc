# Python Perfect World Client,
# All enabled plugins
# (c) GavYur, 2o13

import os

enabled = []

for filename in os.listdir('plugins'):
    if filename.endswith('.py') and filename != '__init__.py':
        enabled.append(filename[:-3])