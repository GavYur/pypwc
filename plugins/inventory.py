# -*- coding: utf-8 -*-

# Python Perfect World Client,
# Inventory plugin
# (c) GavYur, 2o13

import packets
import logging


def handle_inventory(client, inventory):
    if inventory.type == 0:
        client.inventory.update(inventory.slots, inventory.inventory)
    elif inventory.type == 1:
        client.eqinventory.update(inventory.slots, inventory.inventory)


def handle_bank_pass_state(client, state):
    if state.isset:
        password = client.config['bank_password']
        if password:
            packet = packets.cc2s.BankPassword(password)
            client.connection.send(packet)
        else:
            logging.getLogger('main').log(1, 'Bank password is set, but not defined in config file')
    else:
        password = ''
        packet = packets.cc2s.BankPassword(password)
        client.connection.send(packet)


def handle_equip_item(client, packet):
    if (packet.source_inventory == 0) and (packet.recv_inventory == 1):
        item = client.inventory.slot_content(packet.inventory_slot)
        client.inventory.clear_slot(packet.inventory_slot)
        client.eqinventory.fill_slot(packet.eqinventory_slot, item)
    elif (packet.source_inventory == 1) and (packet.recv_inventory == 0):
        item = client.eqinventory.slot_content(packet.eqinventory_slot)
        client.eqinventory.clear_slot(packet.eqinventory_slot)
        client.inventory.fill_slot(packet.inventory_slot, item)


def handle_dropped_item(client, packet):
    if packet.inventory == 0:
        client.inventory.decrease_item(packet.slot, packet.count)
        logging.getLogger('main').log(1, 'Dropped %sx%s item from %s slot of inventory' % (packet.count, packet.item, packet.slot))
    elif packet.inventory == 1:
        client.eqinventory.decrease_item(packet.slot, packet.count)
        logging.getLogger('main').log(1, 'Dropped %sx%s item from %s slot of equip inventory' % (packet.count, packet.item, packet.slot))


def handle_accepted_item(client, packet):
    if packet.inventory == 0:
        client.inventory.fill_slot(packet.slot, {'item': packet.item, 'count': packet.count_all, 'info': bytearray()})
        logging.getLogger('main').log(1, 'Accepted %sx%s item to %s slot of inventory' % (packet.count, packet.item, packet.slot))
    elif packet.inventory == 1:
        client.eqinventory.fill_slot(packet.slot, {'item': packet.item, 'count': packet.count_all, 'info': bytearray()})
        logging.getLogger('main').log(1, 'Accepted %sx%s item to %s slot of equip inventory' % (packet.count, packet.item, packet.slot))


def handle_pickedup_item(client, packet):
    if packet.inventory == 0:
        client.inventory.fill_slot(packet.slot, {'item': packet.item, 'count': packet.count_all, 'info': bytearray()})
        logging.getLogger('main').log(1, 'Picked Up %sx%s item to %s slot of inventory' % (packet.count, packet.item, packet.slot))
    elif packet.inventory == 1:
        client.eqinventory.fill_slot(packet.slot, {'item': packet.item, 'count': packet.count_all, 'info': bytearray()})
        logging.getLogger('main').log(1, 'Picked Up %sx%s item to %s slot of equip inventory' % (packet.count, packet.item, packet.slot))


def handle_money(client, packet):
    client.money = packet.money


def handle_spend_money(client, packet):
    client.money -= packet.money


def handle_pickedup_money(client, packet):
    client.money += packet.money


def handle_item_to_money(client, packet):
    client.inventory.decrease_item(packet.slot, packet.count)
    client.money += packet.money


on_packet = {packets.sc2c.Inventory: [handle_inventory],
             packets.sc2c.BankPasswordState: [handle_bank_pass_state],
             packets.sc2c.EquipItem: [handle_equip_item],
             packets.sc2c.DroppedItem: [handle_dropped_item],
             packets.sc2c.AcceptedItem: [handle_accepted_item],
             packets.sc2c.PickedUpItem: [handle_pickedup_item],
             packets.sc2c.Money: [handle_money],
             packets.sc2c.SpendMoney: [handle_spend_money],
             packets.sc2c.PickedUpMoney: [handle_pickedup_money],
             packets.sc2c.ItemToMoney: [handle_item_to_money]}
