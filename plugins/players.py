# -*- coding: utf-8 -*-

# Python Perfect World Client,
# Players plugin
# Gets player, guild info
# (c) GavYur, 2o13

import time
import logging
import packets
from modules import bytes

levels = {1: 55,
          2: 220,
          3: 495,
          4: 880,
          5: 1400,
          6: 2010,
          7: 2765,
          8: 3640,
          9: 4635,
          10: 5750,
          11: 7040,
          12: 8400,
          13: 9945,
          14: 11620,
          15: 13500,
          16: 15440,
          17: 17595,
          18: 19890,
          19: 22325,
          20: 25000,
          21: 27825,
          22: 30800,
          23: 34040,
          24: 37440,
          25: 41125,
          26: 44980,
          27: 49140,
          28: 53480,
          29: 58145,
          30: 63000,
          31: 68045,
          32: 73600,
          33: 79365,
          34: 85510,
          35: 92050,
          36: 99000,
          37: 106190,
          38: 113810,
          39: 122070,
          40: 130600,
          41: 139810,
          42: 149520,
          43: 159745,
          44: 170720,
          45: 182250,
          46: 194350,
          47: 207270,
          48: 221040,
          49: 235445,
          50: 250750,
          51: 267240,
          52: 284440,
          53: 302895,
          54: 322380,
          55: 343200,
          56: 365120,
          57: 388455,
          58: 413540,
          59: 439845,
          60: 468000,
          61: 514186,
          62: 564426,
          63: 618633,
          64: 677751,
          65: 742105,
          66: 811633,
          67: 887040,
          68: 968679,
          69: 1057360,
          70: 1153044,
          71: 1257081,
          72: 1437950,
          73: 1640333,
          74: 1866865,
          75: 2119163,
          76: 2400221,
          77: 2712606,
          78: 3060712,
          79: 3446869,
          80: 3875270,
          81: 4350544,
          82: 4876697,
          83: 5457952,
          84: 6102480,
          85: 6813012,
          86: 7598978,
          87: 8464893,
          88: 9418742,
          89: 10470584,
          90: 11688300,
          91: 13394199,
          92: 14933072,
          93: 16613613,
          94: 18447312,
          95: 20448750,
          96: 22631232,
          97: 25006212,
          98: 27596408,
          99: 30412503,
          100: 66954000,
          101: 267816000,
          102: 535632000,
          103: 1339080000,
          104: 1750000000,
          105: 0}

def player_list_handler(client, playerlist):
    uids = []
    guilds = []
    equip = []
    for uid in playerlist.players:
        if uid == client.char.id:
            Xcoord = playerlist.players[uid]['X']
            Ycoord = playerlist.players[uid]['Y']
            Zcoord = playerlist.players[uid]['Z']
            client.char.loc.set_coords(Xcoord, Ycoord, Zcoord)
        else:
            uids.append(bytes.from_int(uid, 4))
        gid = playerlist.players[uid]['guildid']
        if gid and gid not in guilds:
            guilds.append(gid)
        if playerlist.players[uid]['get_equip']:
            equip.append(bytes.from_int(uid, 4))
        s_info_uid = playerlist.players[uid]['short_info_uid']
        if s_info_uid:
            packet = packets.c2s.GetPlayerShortInfo(client.char.bid, s_info_uid, 2)
            client.connection.send(packet)
    if uids:
        packet = packets.c2s.GetPlayersAppearance(client.char.bid, uids)
        client.connection.send(packet)
    if guilds:
        packet = packets.c2s.GetGuildsInfo(client.char.bid, guilds)
        client.connection.send(packet)
    if equip:
        packet = packets.cc2s.GetPlayersEquip(equip)
        client.connection.send(packet)

def player_entered_radius(client, player):
    if player.uid == client.char.id:
        client.char.loc.set_coords(player.X, player.Y, player.Z)
    else:
        packet = packets.c2s.GetPlayersAppearance(client.char.bid, [bytes.from_int(player.uid, 4)])
        client.connection.send(packet)
        if player.get_equip:
            packet = packets.cc2s.GetPlayersEquip([bytes.from_int(player.uid, 4)])
            client.connection.send(packet)
        if player.guildid:
            packet = packets.c2s.GetGuildsInfo(client.char.bid, [player.guildid])
            client.connection.send(packet)
        if player.short_info_uid:
            packet = packets.c2s.GetPlayerShortInfo(client.char.bid, player.short_info_uid, 2)
            client.connection.send(packet)

def player_break_radius(client, player):
    if player.uid in client.users_nearby:
        del client.users_nearby[player.uid]

def player_appearance(client, appearance):
    client.users_nearby[appearance.uid] = {'nick': appearance.nick}

def player_short_info(client, info):
    client.users_nearby[info.uid] = {'nick': info.nick}

def wait_nick(client, uid, timeout, call=None):
    cnt = 0
    while (cnt < timeout) and (uid not in client.users_nearby):
        time.sleep(1)
        cnt += 1
    if uid in client.users_nearby:
        if call:
            call(client, client.users_nearby[uid]['nick'])
        return client.users_nearby[uid]['nick']

def get_nick(client, uid, timeout=30, call=None):
    if uid == client.char.bid:
        return client.nickname
    if bytes.to_int(uid) in client.friends:
        return client.friends[bytes.to_int(uid)]['nick']
    elif bytes.to_int(uid) in client.users_nearby:
        return client.users_nearby[bytes.to_int(uid)]['nick']
    else:
        packet = packets.c2s.GetPlayerShortInfo(client.char.bid, uid, 1)
        client.connection.send(packet)
        if call:
            threading.Thread(target=wait_nick, args=(client, bytes.to_int(uid), timeout, call)).start()
        else:
            return wait_nick(client, bytes.to_int(uid), timeout)

def get_uid(client, nick):
    for uid in client.friends:
        if client.friends[uid]['nick'] == nick:
            return bytes.from_int(uid)
    for uid in client.users_nearby:
        if client.users_nearby[uid]['nick'] == nick:
            return bytes.from_int(uid)
    return bytearray([0, 0, 0, 0])

def friend_list_handler(client, friendlist):
    client.friends = friendlist.friends

def friend_status_handler(client, status):
    if status.uid in client.friends:
        client.friends[status.uid]['online'] = status.online

def get_friend_list(client):
    packet = packets.c2s.GetFriendList(client.char.bid)
    client.connection.send(packet)

def get_my_info(client):
    packet = packets.cc2s.GetSelfInfo([1, 1, 0])
    client.connection.send(packet)

def player_main_info_handler(client, info):
    if (client.char.info.hp, client.char.info.maxhp) != (info.hp, info.maxhp):
        client.char.info.hp = info.hp
        client.char.info.maxhp = info.maxhp
        logging.getLogger('main').log(1, 'Hit Points: %d/%d' % (info.hp, info.maxhp))
    if (client.char.info.mp, client.char.info.maxmp) != (info.mp, info.maxmp):
        client.char.info.mp = info.mp
        client.char.info.maxmp = info.maxmp
        logging.getLogger('main').log(1, 'Mana Points: %d/%d' % (info.mp, info.maxmp))
    exp_info = []
    if client.char.level != info.level:
        client.char.level = info.level
        exp_info.append('Level UP! (%d lvl)' % info.level)
    if client.char.info.experience != info.experience:
        client.char.info.experience = info.experience
        maxexp = levels[client.char.level]
        percentage = float(info.experience) / float(maxexp) * 100
        exp_info.append('Experience: %d/%d (%.1f%s)' % (info.experience, maxexp, percentage, '%'))
    if client.char.info.spirit != info.spirit:
        client.char.info.spirit = info.spirit
        exp_info.append('Spirit: %d' % info.spirit)
    if exp_info:
        logging.getLogger('main').log(1, '\n'.join(exp_info))

on_packet = {packets.s2c.PlayerAppearance: [player_appearance],
             packets.s2c.PlayerShortInfo: [player_short_info],
             packets.s2c.FriendList: [friend_list_handler],
             packets.s2c.FriendStatus: [friend_status_handler],
             packets.sc2c.PlayerList: [player_list_handler],
             packets.sc2c.PlayerEnteredRadius: [player_entered_radius],
             packets.sc2c.PlayerBreakRadius: [player_break_radius],
             packets.sc2c.PlayerMainInfo: [player_main_info_handler]}

on_first_container = [get_friend_list, get_my_info]