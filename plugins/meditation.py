# -*- coding: utf-8 -*-

# Python Perfect World Client,
# Meditation plugin
# (c) GavYur, 2o13

import packets
import datetime
import logging

meditation_types = ['standart', 'extended']
meditation_shards = ((35557, 36841), (36636,))


def handle_meditation_info(client, info):
    if client.char.level < 31:
        return
    client.meditation.total = info.available_secs
    client.meditation.mtypes.update(info.times)
    available = datetime.timedelta(seconds=info.available_secs)
    logging.getLogger('main').log(1, 'Available meditation time: %s' % str(available))
    if info.times:
        for mtype in info.times:
            available_time = datetime.timedelta(seconds=info.times[mtype])
            logging.getLogger('main').log(1, 'Available %s meditation time: %s' % (meditation_types[mtype], str(available_time)))
    mtype = meditation_types.index(client.config['meditation_type'])
    if not client.meditation.mtypes[mtype] and client.meditation.total:
        if find_meditation_shards(client, mtype):
            add_meditation_time(client, mtype)
        elif client.config['meditation_downgrade']:
            while mtype > 0:
                mtype -= 1
                if client.meditation.mtypes[mtype]:
                    break
                elif find_meditation_shards(client, mtype):
                    add_meditation_time(client, mtype)
                    break
    meditate(client, mtype)


def handle_meditation_status(client, status):
    if status.status == 0:
        client.meditation.now = False
        logging.getLogger('main').log(1, 'Stop %s meditation' % meditation_types[status.mtype])
    elif status.status == 1:
        client.meditation.now = True
        logging.getLogger('main').log(1, 'Start %s meditation' % meditation_types[status.mtype])


def find_meditation_shards(client, mtype):
    for shard in meditation_shards[mtype]:
        if client.inventory.has_item(shard):
            slot = client.inventory.item_slot(shard)
            return (shard, slot)


def add_meditation_time(client, mtype):
    shard = find_meditation_shards(client, mtype)
    if shard:
        packet = packets.cc2s.IncreaseMeditationTime(mtype, shard[0], shard[1], 1)
        client.connection.send(packet)


def meditate(client, mtype):
    if client.meditation.now:
        return
    if client.meditation.can_meditate(mtype):
        if client.char.loc.dungeon == 150:
            packet = packets.cc2s.SwitchMeditation(mtype, 1)
            client.connection.send(packet)
        else:
            logging.getLogger('main').log(1, 'Character not in Meditation Location')


on_packet = {packets.sc2c.MeditationInfo: [handle_meditation_info],
             packets.sc2c.MeditationStatus: [handle_meditation_status]}
