# -*- coding: utf-8 -*-

# Python Perfect World Client,
# AC plugin
# I don't know what is this, 
# but I tried to simulate client behaviour
# (c) GavYur, 2o13

from modules import bytes
import packets

def handle_ac_rc(client, packet):
    packet = packets.c2s.ACReport(client.char.bid, packet.necessary_bytes)
    client.connection.send(packet)

on_packet = {packets.s2c.ACRemoteCode: [handle_ac_rc]}