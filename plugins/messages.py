# -*- coding: utf-8 -*-

# Python Perfect World Client,
# Messages plugin
# Handle incoming messages, send own
# (c) GavYur, 2o13


from modules import smilies, bytes
import packets
import players
import random
import re


def parse_message(message):
    message = message.encode('unicode_escape')
    attach = re.search(u'(\\\\ue0[0-9]{2})<([0-9]+)><(.*?)>', message)
    while attach:
        result = attach.groups()
        atype = int(result[1])
        if atype == 0:
            smile_part = result[2].split(':')
            smilepack = int(smile_part[0])
            smile = int(smile_part[1])
            smiletext = smilies.smilies[smilepack][smile]
            toreplace = (u'*%s*' % smiletext).encode('unicode_escape')
        elif atype == 1:
            toreplace = result[2][7:]
        else:
            toreplace = result[2]
        message = message.replace(u'%s<%s><%s>' % (result[0], result[1], result[2]), toreplace)
        attach = re.search(u'(\\\\ue0[0-9]{2})<([0-9]+)><(.*?)>', message)
    return message.decode('unicode_escape')


def send_private_message(client, nick, message):
    all_len = len(nick) + len(message) + 2
    if all_len > 80:
        surplace = message[-(all_len - 80):]
        message = message[:-(all_len - 80)]
    else:
        surplace = ''
    msgtype = 7
    touid = players.get_uid(client, nick)
    uid = bytes.to_int(touid)
    if uid in client.friends:
        flag = 0
    else:
        flag = 85
    packet = packets.c2s.SendPrivateMessage(msgtype, flag, client.char.bid, client.char.name, nick, touid, message, touid)
    client.connection.send(packet)
    text = u'/%s %s' % (nick, message)
    if client.chatlog:
        client.chatlog.log('private', '%s: %s' % (client.char.name, text))
    if surplace:
        send_private_message(client, nick, surplace)


def send_private_popup_message(client, nick, message):
    if len(message) > 80:
        surplace = message[-(len(message) - 80):]
        message = message[:-(len(message) - 80)]
    else:
        surplace = ''
    msgtype = 8
    flag = 182
    touid = players.get_uid(client, nick)
    uid = bytes.to_int(touid)
    end = bytearray([0, 100, 40, 138])
    packet = packets.c2s.SendPrivateMessage(msgtype, flag, client.char.bid, client.char.name, nick, touid, message, end)
    client.connection.send(packet)
    text = u'/%s %s' % (nick, message)
    if client.chatlog:
        client.chatlog.log('private-popup', '%s: %s' % (client.char.name, text))
    if surplace:
        send_private_popup_message(client, nick, surplace)


def send_chat_message(client, message):
    if len(message) > 80:
        surplace = message[-(len(message) - 80):]
        message = message[:-(len(message) - 80)]
    else:
        surplace = ''
    packet = packets.c2s.SendChatMessage(client.char.bid, message)
    client.connection.send(packet)
    if client.chatlog:
        client.chatlog.log('main', '%s: %s' % (client.char.name, message))
    if surplace:
        send_chat_message(client, surplace)


def send_guild_message(client, message):
    if len(message) > 80:
        surplace = message[-(len(message) - 80):]
        message = message[:-(len(message) - 80)]
    else:
        surplace = ''
    packet = packets.c2s.SendGuildMessage(client.char.bid, message)
    client.connection.send(packet)
    if surplace:
        send_guild_message(client, surplace)


def send_user_info_message(client, uid):
    level = client.char.level
    locid = client.char.loc.id
    end = bytearray([36, 234, 235, 56]) # what is this bytes?
    levelmsg = packets.c2s.SendPrivateMessage(4, 0, client.char.bid, client.char.name, '', uid, 'L%d' % level, end)
    client.connection.send(levelmsg)
    locmsg = packets.c2s.SendPrivateMessage(4, random.randrange(256), client.char.bid, client.char.name, '', uid, 'A%d' % locid, end)
    client.connection.send(locmsg)


def send_user_info_request(client, nick, uid):
    end = bytearray([0, 132, 22, 200])
    packet = packets.c2s.SendPrivateMessage(4, 182, client.char.bid, client.char.name, nick, uid, 'R', end)
    client.connection.send(packet)


def handle_chat_message(client, message):
    if message.type == 0:
        nick = players.get_nick(client, message.uid)
        if client.chatlog:
            client.chatlog.log('main', '%s: %s' % (nick, parse_message(message.text)))
    elif message.type == 7:
        nick = players.get_nick(client, message.uid)
        if client.chatlog:
            client.chatlog.log('trade', '%s: %s' % (nick, parse_message(message.text)))
    elif message.type == 9:
        if client.chatlog:
            client.chatlog.log('system', parse_message(message.text))


def handle_world_message(client, message):
    if not client.chatlog:
        return
    if message.type == 1:
        client.chatlog.log('world', '%s: %s' % (message.nick, parse_message(message.text)))


def handle_private_message(client, message):
    if message.type == 0:
        if client.chatlog:
            client.chatlog.log('private', '%s: %s' % (message.nick, parse_message(message.text)))
        if client.config['autoreply']:
            send_private_message(client, message.nick, client.config['autoreply'])
    elif message.type == 2:
        uid = bytes.to_int(message.uid)
        if uid not in client.friends_info:
            send_user_info_request(client, message.nick, message.uid)
        if client.chatlog:
            client.chatlog.log('private-popup', '%s: %s' % (message.nick, parse_message(message.text)))
        if client.config['autoreply']:
            send_private_popup_message(client, message.nick, client.config['autoreply'])
    elif message.type == 4:
        if message.text == 'R':
            send_user_info_message(client, message.uid)
        else:
            uid = bytes.to_int(message.uid)
            if uid not in client.friends_info:
                client.friends_info[uid] = {}
            if message.text.startswith('L'):
                client.friends_info[uid]['level'] = int(message.text[1:])
            elif message.text.startswith('A'):
                client.friends_info[uid]['locid'] = int(message.text[1:])


def handle_guild_message(client, message):
    nick = players.get_nick(client, message.uid)
    if client.chatlog:
        client.chatlog.log('guild', '%s: %s' % (nick, parse_message(message.text)))


def get_saved_messages(client):
    packet = packets.c2s.GetSavedMessages(client.char.bid)
    client.connection.send(packet)


def handle_saved_messages(client, messages):
    for message in messages.messages:
        if message['type'] == 2:
            send_user_info_request(client, message['nick'], message['uid'])
            if client.chatlog:
                client.chatlog.log('offline-message', '%s: %s' % (message['nick'], parse_message(message['text'])))


on_packet = {packets.s2c.WorldMessage: [handle_world_message],
             packets.s2c.ChatMessage: [handle_chat_message],
             packets.s2c.PrivateMessage: [handle_private_message],
             packets.s2c.GuildMessage: [handle_guild_message],
             packets.s2c.SavedMessages: [handle_saved_messages]}

on_first_container = [get_saved_messages]