# Python Perfect World Client,
# Config module
# (c) GavYur, 2o13


import yaml


class IncorrectConfig(Exception): pass


class Config:
    def __init__(self, filename=None, required=[], default={}):
        if filename:
            cfile = file(filename, 'r')
            self.__config = yaml.load(cfile)
            cfile.close()
        else:
            self.__config = {}
        for parameter in required:
            if not parameter in self.__config:
                raise IncorrectConfig('Required config parameter "%s" not found' % parameter)
        self.default_params = default

    def __getitem__(self, key):
        if key in self.__config:
            return self.__config[key]
        elif key in self.default_params:
            return self.default_params[key]
        else:
            raise KeyError(key)