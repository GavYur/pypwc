def point_in_poly(coords, poly):
    if coords in poly:
        return True
    n = len(poly)
    inside = False
    p1x, p1y = poly[0]
    for i in range(n+1):
        p2x, p2y = poly[i % n]
        if coords[1] > min(p1y, p2y):
            if coords[1] <= max(p1y, p2y):
                if coords[0] <= max(p1x, p2x):
                    if p1y != p2y:
                        xinters = (coords[1] - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                    if p1x == p2x or coords[0] <= xinters:
                        inside = not inside
        p1x, p1y = p2x, p2y
    return inside