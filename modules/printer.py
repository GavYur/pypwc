from colorama import init, Fore, Back, Style
from threading import Lock
import sys


class Printer:
    def __init__(self):
        self.print_lock = Lock()

    def sprint(self, *objects, **kwargs):
        sep = kwargs.get('sep', ' ')
        if sep == None:
            sep = ' '
        if not isinstance(sep, basestring):
            raise TypeError('sep must be None, str or unicode, not ' + type(sep).__name__)

        end = kwargs.get('end', '\n')
        if end == None:
            end = ' '
        if not isinstance(end, basestring):
            raise TypeError('end must be None, str or unicode, not ' + type(end).__name__)

        pfile = sys.stdout
        objects = sep.join(map(unicode, objects))

        color = kwargs.get('color', '')
        if color:
            col_start = getattr(Fore, color.upper(), '')
            if not col_start:
                raise ValueError('color ' + color + ' not found')
            col_end = Fore.RESET
            objects = col_start + objects + col_end

        back = kwargs.get('back', '')
        if back:
            back_start = getattr(Back, back.upper(), '')
            if not back_start:
                raise ValueError('background color ' + back + ' not found')
            back_end = Back.RESET
            objects = back_start + objects + back_end

        style = kwargs.get('style', '')
        if style:
            style_start = getattr(Style, style.upper(), '')
            if not style_start:
                raise ValueError('style ' + style + ' not found')
            style_end = Style.RESET_ALL
            objects = style_start + objects + style_end

        with self.print_lock:
            pfile.write(objects + end)


init()
printer = Printer()
sprint = printer.sprint