# Python Perfect World Client,
# Additional logging handlers
# (c) GavYur, 2o13


import time
import threading
import traceback
import logging
import os
import sys
from logging.handlers import BaseRotatingHandler


class DateDirectoryFileHandler(BaseRotatingHandler):
    def __init__(self, filename, format='%d.%m.%Y', encoding=None, delay=False, utc=False):
        self.utc = utc
        self.date_format = format
        self.filename_format = filename
        fname = self.make_filename()
        self.check_directories(fname)
        super(BaseRotatingHandler, self).__init__(fname, 'a', encoding, delay)

    def make_filename(self, utime=None):
        if not utime:
            utime = time.time()
        if self.utc:
            t = time.gmtime(utime)
        else:
            t = time.localtime(utime)
        return os.path.abspath(self.filename_format % {'date': time.strftime(self.date_format, t)})

    def check_directories(self, filename=None):
        if not filename:
            filename = self.baseFilename
        dirname = os.path.dirname(filename)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

    def shouldRollover(self, record):
        if self.make_filename() != self.baseFilename:
            return 1
        return 0

    def doRollover(self):
        if self.stream:
            self.stream.close()
            self.stream = None
        self.baseFilename = self.make_filename()
        self.check_directories()
        self.stream = self._open()


def install_exceptions_logging(logger_name):
    def excepthook(etype, value, etraceback):
        exception = ''.join(traceback.format_exception(etype, value, etraceback)).strip()
        logging.getLogger(logger_name).log(1, exception)
        sys.__excepthook__(etype, value, etraceback)
    sys.excepthook = excepthook


def threading_excepthook():
    run_old = threading.Thread.run
    def run(*args, **kwargs):
        try:
            run_old(*args, **kwargs)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            sys.excepthook(*sys.exc_info())
    threading.Thread.run = run
