# -*- coding: utf-8 -*-


import os
import logging
import time
import locations
import geometry
import bytes
import logging_addons


genders = ('male', 'female')
classes = ('Blademaster', 'Wizard', 'Psychic', 'Venomancer', 'Barbarian', 'Assassin', 'Archer', 'Cleric', 'Seeker', 'Mystic')


class CharacterBase(object):
    def __init__(self, uid=0, genderid=0, race=0, pclass=0, level=0, name=''):
        if isinstance(uid, bytearray):
            self.bid = uid
            self.id = bytes.to_int(uid)
        else:
            self.bid = bytes.from_int(uid)
            self.id = uid
        self.gender = genders[genderid]
        self.race = race
        self.pclass = classes[pclass]
        self.level = level
        self.name = name


class SelfCharacter(CharacterBase):
    def __init__(self, uid=0, genderid=0, race=0, pclass=0, level=0, name=''):
        super(SelfCharacter, self).__init__(uid, genderid, race, pclass, level, name)
        self.loc = Location(print_change=True)
        self.info = AdditionalInformation()


class Location:
    def __init__(self, dungeon=0, coords={'x': None, 'y': None, 'z': None}, locid=0, locname='', print_change=False):
        self.dungeon = dungeon
        self.coords = coords
        self.id = locid
        self.name = locname
        self.print_change = print_change

    def get_locinfo(self):
        if not self.dungeon in locations.locs:
            return
        coords2d = (self.coords['x'], self.coords['y'])
        for loc in locations.locs[self.dungeon]:
            if geometry.point_in_poly(coords2d, loc['polygon']):
                return loc

    def update_locinfo(self):
        if not self.coords['x'] or not self.coords['y'] or not self.dungeon:
            return
        loc = self.get_locinfo()
        if loc and ((loc['id'] != self.id) or (loc['name'] != self.name)):
            self.id = loc['id']
            self.name = loc['name']
            if self.print_change:
                logging.getLogger('main').log(1, 'Change location to %s' % self.name)

    def set_coords(self, X, Y, Z):
        self.coords = {'x': X, 'y': Y, 'z': Z}
        self.update_locinfo()

    def set_dungeon(self, dungeon):
        self.dungeon = dungeon
        self.update_locinfo()


class AdditionalInformation:
    def __init__(self, hp=None, maxhp=None, mp=None, maxmp=None, exp=None, spirit=None):
        self.hp = hp
        self.maxhp = maxhp
        self.mp = mp
        self.maxmp = maxmp
        self.experience = exp
        self.spirit = spirit


class MeditationTime:
    def __init__(self, total=0, mtypes={}):
        self.total = total
        self.mtypes = mtypes
        self.now = False

    def can_meditate(self, mtype):
        if self.total:
            if (mtype in self.mtypes) and (self.mtypes[mtype]):
                return True
        return False


class FullInventory(Exception): pass


class Inventory(object):
    def __init__(self, size=32, content={}):
        self.update(size, content)

    def update(self, size, content):
        self.content = [None] * size
        for slot in content:
            self.content[slot] = content[slot]

    def slot_content(self, slot):
        return self.content[slot]

    def clear_slot(self, slot):
        self.fill_slot(slot, None)

    def fill_slot(self, slot, content):
        self.content[slot] = content

    def decrease_item(self, slot, cnt):
        self.content[slot]['count'] -= cnt
        if self.content[slot]['count'] <= 0:
            self.clear_slot(slot)

    def update_slot(self, slot, cnt=None, info=None, item=None):
        if cnt:
            self.content[slot]['count'] = cnt
        if info:
            self.content[slot]['info'] = info
        if item:
            self.content[slot]['item'] = item

    def has_item(self, item):
        if self.item_slot(item) != None:
            return True
        else:
            return False

    def item_slot(self, item):
        for slot, cell in enumerate(self.content):
            if cell and cell['item'] == item:
                return slot


class EquipInventory(Inventory):
    slots_names = ('weapon', # пушка
                   'helmet', # шлем
                   'necklace', # ожерелье
                   'robe', # накидка
                   'shirt', # верх 
                   'belt', # пояс
                   'pants', # штаны
                   'footwear', # сапоги
                   'wrist_bracer', # браслеты
                   'left_ring', # левое кольцо
                   'right_ring', # правое кольцо
                   'projectile', # боеприпасы
                   'fly', # крылья
                   'body_fashion', # стиль, верх
                   'legwear_fashion', # стиль, штаны
                   'footwear_fashion', # стиль, сапоги
                   'arm_fashion', # стиль, нарукавники
                   'attack_charm', # знак атаки
                   'tome', # книга
                   'smilies', # смайлы
                   'guardian_charm', # амулет
                   'spirit_charm', # идол
                   'bless_box', # нижняя книга
                   'genie', # джинн
                   'vendor_shop', # торговая лавка
                   'head_fashion', # стиль, прическа
                   'vanguard_badge', # грамота альянса
                   'mark_of_might1', # печать воителя 1
                   'mark_of_might2')  # печать воителя 2

    def __init__(self, content={}, size=29):
        self.update(size, content)

    def __getattr__(self, name):
        if name in self.slots_names:
            return self.slot_content(self.slots_names.index(name))
        else:
            return super(EquipInventory, self).__getattr__(name)

    def __delattr__(self, name):
        if name in self.slots_names:
            self.clear_slot(self.slots_names.index(name))
        else:
            super(EquipInventory, self).__delattr__(name)

    def __setattr__(self, name, value):
        if name in self.slots_names:
            self.fill_slot(self.slots_names.index(name), value)
        else:
            super(EquipInventory, self).__setattr__(name, value)


class ChatLogger:
    def __init__(self, directory, account):
        self.directory = os.path.normcase(directory)
        self.account = account

    def get_logger(self, mtype):
        mtype = mtype.lower()
        logger = logging.getLogger('messages.%s.%s' % (self.account, mtype))
        logger.setLevel(1)
        if not logger.handlers:
            path = os.path.join(self.directory, '%(date)s', '%s.log' % mtype)
            handler = logging_addons.DateDirectoryFileHandler(path)
            logger.addHandler(handler)
        return logger

    def log(self, mtype, text):
        logger = self.get_logger(mtype)
        text = '[%s] %s' % (time.strftime('%H:%M:%S'), text)
        logger.log(1, text)