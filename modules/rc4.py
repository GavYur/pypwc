# Copyright (C) 2012 Bo Zhu http://about.bozhu.me
# Edited by GavYur, 2o13

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

from threading import Lock

class rc4:
    def __init__(self, key):
        self.key = key
        self.bkey = [ord(c) for c in key]
        self.skey = self.schedule_key()
        self.stream_lock = Lock()
        self.stream = self.get_stream()

    def schedule_key(self):
        keylength = len(self.bkey)
        S = range(256)
        j = 0
        for i in range(256):
            j = (j + S[i] + self.bkey[i % keylength]) % 256
            S[i], S[j] = S[j], S[i]
        return S

    def get_stream(self):
        i = 0
        j = 0
        while True:
            i = (i + 1) % 256
            j = (j + self.skey[i]) % 256
            self.skey[i], self.skey[j] = self.skey[j], self.skey[i]
            K = self.skey[(self.skey[i] + self.skey[j]) % 256]
            yield K

    def encode_symbol(self, symbol):
        return chr(ord(symbol) ^ self.stream.next())

    def __encode(self, data):
        result = ''
        for symbol in data:
            result += self.encode_symbol(symbol)
        return result

    def encode(self, data):
        with self.stream_lock:
            result = self.__encode(data)
        return result

    decode = encode