# -*- coding: utf-8 -*-

# Python Perfect World Client,
# Core
# (c) GavYur, 2o13

import sys
import os
import socket
import hashlib
import select
import hmac
import threading
import random
import time
import importlib
import traceback
import logging

import config
import rc4
import packets
import bytes
import mppc
import cuint32
import yaml
import objects
import plugins
import logging_addons


servers = {u'Орион': ('link1.pwonline.ru', 29000),
           u'Вега': ('link2.pwonline.ru', 29000),
           u'Сириус': ('link3.pwonline.ru', 29000),
           u'Мира': ('link4.pwonline.ru', 29000),
           u'Таразед': ('link5.pwonline.ru', 29000),
           u'Альтаир': ('link6.pwonline.ru', 29000),
           u'Процион': ('link7.pwonline.ru', 29000),
           u'Астра': ('link8.pwonline.ru', 29000),
           u'Пегас': ('link9.pwonline.ru', 29000),
           u'Антарес': ('link10.pwonline.ru', 29000),
           u'Адара': ('link11.pwonline.ru', 29000),
           u'Феникс': ('link12.pwonline.ru', 29000),
           u'Лиридан': ('link13.pwonline.ru', 29000),
           u'Омега': ('link15.pwonline.ru', 29000)}


class Connection:
    def __init__(self, server, callbacks=[]):
        self.server = server
        self.callbacks = callbacks
        self.rc4encoder = None
        self.rc4decoder = None
        self.MPPC = None
        self.alive = False
        self.socket_lock = threading.Lock()

    def connect(self):
        logging.getLogger('main').log(1, 'Connecting to %s:%s' % self.server)
        self.socket = socket.create_connection(self.server)
        logging.getLogger('main').log(1, 'Connection established')
        self.alive = True
        self.start_listening()

    def can_write(self, timeout=0):
        if select.select([], [self.socket], [], timeout)[1]:
            return True
        else:
            return False

    def can_read(self, timeout=0):
        if select.select([self.socket], [], [], timeout)[0]:
            return True
        else:
            return False

    def recv_data(self, buf):
        with self.socket_lock:
            data = self.socket.recv(buf)
        if not data:
            return
        if self.rc4decoder:
            data = self.rc4decoder.decode(data)
            if not self.MPPC:
                self.MPPC = mppc.MPPC()
            data = self.MPPC.decompress(data)
        return data

    def read_packet(self, start='', buf=2048):
        if start:
            data = start
        else:
            data = self.recv_data(buf)
            if not data:
                return
            data = bytearray(data)
        packet = packets.Packet(raw=data)
        while packet.not_enough:
            data = self.recv_data(buf)
            if not data:
                raise Exception('Not enough packet')
            data = bytearray(data)
            packet.add_data(data)
        return packet

    def start_listening(self, timeout=1):
        while self.alive:
            if self.can_read(timeout):
                packet = self.read_packet()
                if not packet:
                    break
                surplus = packet.get_surplus()
                for function in self.callbacks:
                    threading.Thread(target=function, args=(packet,)).start()
                while surplus:
                    packet = self.read_packet(surplus)
                    surplus = packet.get_surplus()
                    for function in self.callbacks:
                        threading.Thread(target=function, args=(packet,)).start()

    def send(self, data):
        if isinstance(data, packets.InContainerPacket):
            data = packets.c2s.PacketsContainer([data])
        if isinstance(data, packets.Packet):
            data = str(data)
        if self.rc4encoder:
            data = self.rc4encoder.encode(data)
        datalen = len(data)
        totalsent = 0
        while totalsent < datalen:
            if self.can_write(1):
                with self.socket_lock:
                    sent = self.socket.send(data[totalsent:])
                if not sent:
                    self.disconected()
                    break
                totalsent += sent

    def disconnected(self):
        logging.getLogger('main').log(1, 'Disconnected!')
        self.disconnect()

    def disconnect(self):
        self.alive = False
        self.socket.close()
        sys.exit()

class Client:
    def __init__(self, cfg):
        self.login = cfg['login'].lower()
        self.nickname = cfg['character_name']
        self.force = cfg['force']
        server = cfg['server']
        self.config = cfg
        if isinstance(server, basestring):
            server = servers[server.title()]
        elif isinstance(server, list):
            server = tuple(server)
        self.char = None
        self.inventory = objects.Inventory()
        self.eqinventory = objects.EquipInventory()
        self.meditation = objects.MeditationTime()
        if cfg['chatlogs']:
            self.chatlog = objects.ChatLogger(cfg['chatlogs'], self.login)
        else:
            self.chatlog = None
        self.money = 0
        self.users_nearby = {}
        self.friends_info = {}
        self.friends = {}
        self.first_container = False
        self.keepalive_timer = None
        self.imported_modules = {}
        self.connection = Connection(server, [self.handle_packet])
        self.connect = self.connection.connect
        self.callbacks = {'on_packet': {},
                          'on_array_packet': {},
                          'on_enter_world': [],
                          'on_first_container': []}
        self.import_plugins()


    def import_plugins(self):
        for module_name in plugins.enabled:
            try:
                module = importlib.import_module('plugins.%s' % module_name)
            except:
                logging.getLogger('error').log(1, 'Can\'t import module %s, ignore it.\n%s' % (module_name, traceback.format_exc().strip()))
            else:
                self.imported_modules[module_name] = module
                packet = getattr(module, 'on_packet', {})
                enter_world = getattr(module, 'on_enter_world', [])
                first_container = getattr(module, 'on_first_container', [])
                on_packet = {}
                on_array_packet = {}
                for callback in packet:
                    if issubclass(callback, packets.InContainerPacket):
                        on_array_packet[callback] = packet[callback]
                    elif issubclass(callback, packets.Packet):
                        on_packet[callback] = packet[callback]
                self.add_callback('on_packet', on_packet)
                self.add_callback('on_array_packet', on_array_packet)
                self.add_callback('on_enter_world', enter_world)
                self.add_callback('on_first_container', first_container)
        if self.imported_modules:
            logging.getLogger('main').log(1, 'Imported %d plugins: %s' % (len(self.imported_modules), ', '.join(self.imported_modules)))
        else:
            logging.getLogger('main').log(1, 'No imported plugins')


    def add_callback(self, event, callback):
        if isinstance(self.callbacks[event], dict):
            for key in callback:
                if key not in self.callbacks[event]:
                    self.callbacks[event][key] = []
                self.callbacks[event][key] += callback[key]
        elif isinstance(self.callbacks[event], list):
            self.callbacks[event] += callback

    def send_keepalive(self):
        if not self.connection.alive:
            return
        timeout = self.config['keepalive_timeout']
        packet = packets.c2s.KeepAlive()
        self.connection.send(packet)
        self.keepalive_timer = threading.Timer(timeout, self.send_keepalive)
        self.keepalive_timer.start()

    def handle_packet(self, packet):
        if packet.type in packets.s2c.ids:
            packet = packets.s2c.ids[packet.type](packet.buffer)
        for function in self.callbacks['on_packet'].get(type(packet), []):
            threading.Thread(target=function, args=(self, packet)).start()
        if isinstance(packet, packets.s2c.ServerInfo):
            logging.getLogger('main').log(1, 'Server version: %s' % packet.strversion)
            if self.config['password']:
                loginhash = hashlib.md5(self.login + self.config['password']).digest()
            elif self.config['logpass']:
                loginhash = self.config['logpass'].decode('hex')
            self.hash = hmac.new(loginhash, packet.key).digest()
            packet = packets.c2s.LogginAnnounce(self.login, self.hash)
            self.connection.send(packet)
        elif isinstance(packet, packets.s2c.SMKey):
            rc4encode = hmac.new(self.login)
            rc4encode.update(self.hash + packet.key)
            self.connection.rc4encoder = rc4.rc4(rc4encode.digest())
            key = bytearray()
            for x in xrange(0, 16):
                key.append(random.randrange(0, 256))
            rc4decode = hmac.new(self.login)
            rc4decode.update(self.hash + key)
            self.connection.rc4decoder = rc4.rc4(rc4decode.digest())
            packet = packets.c2s.CMKey(key, self.force)
            self.connection.send(packet)
        elif isinstance(packet, packets.s2c.OnlineAnnounce):
            self.account_key = packet.key
            slot = bytearray([0xff, 0xff, 0xff, 0xff])
            packet = packets.c2s.RoleList(self.account_key, slot)
            self.connection.send(packet)
        elif isinstance(packet, packets.s2c.LastLogin):
            login_time = time.localtime(packet.time)
            strtime = time.strftime('%d.%m.%Y, %H:%M:%S', login_time)
            log_msg = 'Last login: %s\nLast login IP: %s\nCurrent IP: %s' % (strtime, packet.last_ip, packet.current_ip)
            logging.getLogger('main').log(1, log_msg)
        elif isinstance(packet, packets.s2c.RoleListRe):
            if packet.ischar:
                if packet.name == self.nickname:
                    self.char = objects.SelfCharacter(packet.charid, packet.gender, packet.race, packet.pclass, packet.level, packet.name)
                    log_msg = 'Logging as %s\nClass: %s\nGender: %s\nLevel: %d' % (self.char.name, self.char.pclass, self.char.gender, self.char.level)
                    logging.getLogger('main').log(1, log_msg)
                packet = packets.c2s.RoleList(packet.account_key, packet.next_slotid)
                self.connection.send(packet)
            elif self.char:
                packet = packets.c2s.SelectRole(self.char.bid)
                self.connection.send(packet)
            else:
                logging.getLogger('error').log(1, 'Character with name %s not found' % self.nickname)
        elif isinstance(packet, packets.s2c.SelectRoleRe):
            logging.getLogger('main').log(1, 'Entering world')
            packet = packets.c2s.EnterWorld(self.char.bid)
            self.connection.send(packet)
            self.send_keepalive()
            for function in self.callbacks['on_enter_world']:
                threading.Thread(target=function, args=(self,)).start()
        elif isinstance(packet, packets.s2c.PacketsContainer):
            if not self.first_container:
                self.first_container = True
                for function in self.callbacks['on_first_container']:
                    threading.Thread(target=function, args=(self,)).start()
            for cpacket in packet.packets:
                self.handle_array_packet(cpacket)
        elif isinstance(packet, packets.s2c.ErrorInfo):
            logging.getLogger('error').log(1, 'Error info received: %s' % packet.message)
            self.disconnect()

    def handle_array_packet(self, packet):
        if packet.type in packets.sc2c.ids:
            packet = packets.sc2c.ids[packet.type](packet.buffer)
        for function in self.callbacks['on_array_packet'].get(type(packet), []):
            threading.Thread(target=function, args=(self, packet)).start()

    def disconnect(self):
        if self.keepalive_timer:
            self.keepalive_timer.cancel()
        self.connection.disconnect()


def setup_logging(directory):
    loggers = ['error', 'main']
    for logger_name in loggers:
        logger = logging.getLogger(logger_name)
        logger.setLevel(1)
        if directory:
            path = os.path.join(directory, '%(date)s', '%s.log' % logger_name)
            handler = logging_addons.DateDirectoryFileHandler(path)
            handler.setFormatter(logging.Formatter('[%(asctime)s] %(message)s', '%H:%M:%S'))
        else:
            handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(handler)


def main(config_file):
    cfg = config.Config(config_file,
                        ['login', 'server', 'character_name'],
                        {'password': '',
                         'logpass': '',
                         'keepalive_timeout': 15,
                         'force': False,
                         'autoreply': '',
                         'chatlogs': '',
                         'syslogs': '',
                         'meditation_type': '',
                         'meditation_downgrade': True,
                         'bank_password': ''})
    setup_logging(cfg['syslogs'])
    logging_addons.threading_excepthook()
    logging_addons.install_exceptions_logging('error')
    if not (cfg['password'] or cfg['logpass']):
        logging.getLogger('error').log(1, 'You must specify your password or login+password md5 hash in config file')
        return
    pwc = Client(cfg)
    try:
        pwc.connect()
    except KeyboardInterrupt:
        logging.getLogger('main').log(1, 'Got Ctrl+C, shutting down...')
        pwc.disconnect()