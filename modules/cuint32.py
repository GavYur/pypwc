def bytes_to_int(bytes):
    result = 0
    for i in xrange(0, len(bytes)):
        result |= bytes[i] << (i * 8)
    return result

def int_to_bytes(integer):
    x = '%x' % (integer,)
    return (('0' * (len(x) % 2)) + x).decode('hex')

def byte_to_int(packet):
    getbyte = lambda: packet.next()
    code = getbyte()
    if code & 0xe0 == 0xe0:
        bytes = [code, getbyte(), getbyte(), getbyte()]
        bytes.reverse()
        return bytes_to_int(bytes)
    elif code & 0xe0 == 0xc0:
        bytes = [code, getbyte(), getbyte(), getbyte()]
        bytes.reverse()
        return bytes_to_int(bytes) & 0x1fffffff
    elif code & 0xe0 in [0x80, 0xa0]:
        return bytes_to_int([getbyte(), code]) & 0x3fff
    else:
        return code

def int_to_byte(integer):
    if integer <= 0x7f:
        return chr(integer)
    elif integer <= 0x3fff:
        return int_to_bytes(integer + 0x8000)
    elif integer <= 0x1fffffff:
        return int_to_bytes(integer + 0xc0000000)
    return chr(integer)