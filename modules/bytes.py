import struct

def from_int(integer, size=4, reverse=False):
    if size == 1:
        typ = 'B' # char
    elif size == 2:
        typ = 'H' # short
    elif size == 4:
        typ = 'I' # int
    elif size == 8:
        typ = 'Q' # long long
    else:
        raise
    result = bytearray(struct.pack('!' + typ, integer))
    if reverse:
        result.reverse()
    return result

def to_float(bytearr, reverse=False):
    if reverse:
        bytearr.reverse()
    size = len(bytearr)
    if size == 4:
        typ = 'f'
    elif size == 8:
        typ = 'd'
    else:
        raise
    return struct.unpack('!' + typ, str(bytearr))[0]

def to_int(bytearr, reverse=False):
    if reverse:
        bytearr.reverse()
    size = len(bytearr)
    if size == 1:
        typ = 'B' # char
    elif size == 2:
        typ = 'H' # short
    elif size == 4:
        typ = 'I' # int
    elif size == 8:
        typ = 'Q' # long long
    else:
        raise
    return struct.unpack('!' + typ, str(bytearr))[0]

def to_unicode(bytearr):
    bytearr[1::2], bytearr[::2] = bytearr[::2], bytearr[1::2]
    result = u''
    for ind, code in enumerate(bytearr):
        if ind % 2:
            result += u'%.2x' % code
        else:
            result += u'\\u%.2x' % code
    return result.decode('unicode_escape')

def from_unicode(string, reverse=False):
    bytearr = bytearray()
    for symbol in string:
        escaped = symbol.encode('unicode_escape').replace('\\u', '')
        if symbol == escaped:
            bytearr += bytearray([0, ord(symbol)])
        else:
            bytearr += bytearray(escaped.decode('hex'))
    if reverse:
        bytearr[1::2], bytearr[::2] = bytearr[::2], bytearr[1::2]
    return bytearr

def read_n(stream, n, reverse=False):
    if n == 1:
        return stream.next()
    result = bytearray()
    for x in xrange(n):
        result.append(stream.next())
    if reverse:
        result.reverse()
    return result