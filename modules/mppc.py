def int_overflow(val):
    if val > int(2**31 - 1):
        val = int((val)&(2**32-1))
    return val

class MPPC:
    def __init__(self):
        self.m_PackedOffset = 0
        self.m_Stage = 0
        self.m_Packed = []
        self.m_Unpacked = []

    def decompress(self, data):
        result = ''
        for byte in data:
            decompressed = self.add_byte(ord(byte))
            for dbyte in decompressed:
                result += chr(dbyte)
        return result

    def has_bits(self, cnt):
        return (len(self.m_Packed) * 8 - self.m_PackedOffset) >= cnt

    def get_packed_bits(self, cnt):
        if cnt > 16:
            return 0
        if not self.has_bits(cnt):
            print 'Unpack bit stream overflow'
        all_bit_cnt = cnt + self.m_PackedOffset
        all_byte_cnt = (all_bit_cnt + 7) / 8
        V = 0
        for i in xrange(0, all_byte_cnt):
            V |= self.m_Packed[i] << (24 - i * 8)
            V = int_overflow(V)
        V <<= self.m_PackedOffset
        V = int_overflow(V)
        V >>= 32 - cnt
        V = int_overflow(V)
        self.m_PackedOffset += cnt
        free_bytes = self.m_PackedOffset / 8
        if free_bytes != 0:
            del self.m_Packed[0:free_bytes]
        self.m_PackedOffset %= 8
        return V

    def copy(self, shift, size, unpacked_chunk):
        for i in xrange(0, size):
            PIndex = len(self.m_Unpacked) - shift
            if PIndex >= 0:
                B = self.m_Unpacked[PIndex]
                self.m_Unpacked.append(B)
                unpacked_chunk.append(B)
            else:
                pass
        return unpacked_chunk

    def add_byte(self, inb):
        self.m_Packed.append(inb)
        unpacked_chunk = []
        while True:
            if self.m_Stage == 0:
                if self.has_bits(4):
                    if self.get_packed_bits(1) == 0:
                        # 0-xxxxxxx
                        self.m_Code1 = 1
                        self.m_Stage = 1
                        continue
                    elif self.get_packed_bits(1) == 0:
                        # 10-xxxxxxx
                        self.m_Code1 = 2
                        self.m_Stage = 1
                        continue
                    elif self.get_packed_bits(1) == 0:
                        # 110-xxxxxxxxxxxxx-*
                        self.m_Code1 = 3
                        self.m_Stage = 1
                        continue
                    elif self.get_packed_bits(1) == 0:
                        # 1110-xxxxxxxx-*
                        self.m_Code1 = 4
                        self.m_Stage = 1
                        continue
                    else:
                        # 1111-xxxxxx-*
                        self.m_Code1 = 5
                        self.m_Stage = 1
                        continue
                else:
                    break
            elif self.m_Stage == 1:
                if self.m_Code1 == 1:
                    if self.has_bits(7):
                        outb = self.get_packed_bits(7)
                        unpacked_chunk.append(outb)
                        self.m_Unpacked.append(outb)
                        self.m_Stage = 0
                        continue
                    else:
                        break
                elif self.m_Code1 == 2:
                    if self.has_bits(7):
                        outb = self.get_packed_bits(7) | 0x80
                        unpacked_chunk.append(outb)
                        self.m_Unpacked.append(outb)
                        self.m_Stage = 0
                        continue
                    else:
                        break
                elif self.m_Code1 == 3:
                    if self.has_bits(13):
                        self.m_Shift = self.get_packed_bits(13) + 0x140
                        self.m_Stage = 2
                        continue
                    else:
                        break
                elif self.m_Code1 == 4:
                    if self.has_bits(8):
                        self.m_Shift = self.get_packed_bits(8) + 0x40
                        self.m_Stage = 2
                        continue
                    else:
                        break
                elif self.m_Code1 == 5:
                    if self.has_bits(6):
                        self.m_Shift = self.get_packed_bits(6)
                        self.m_Stage = 2
                        continue
                    else:
                        break
            elif self.m_Stage == 2:
                if self.m_Shift == 0:
                    if self.m_PackedOffset != 0:
                        self.m_PackedOffset = 0
                        del self.m_Packed[0]
                    self.m_Stage = 0
                    continue
                self.m_Code2 = 0
                self.m_Stage = 3
                continue
            elif self.m_Stage == 3:
                if self.has_bits(1):
                    if self.get_packed_bits(1) == 0:
                        self.m_Stage = 4
                        continue
                    else:
                        self.m_Code2 += 1
                        continue
                else:
                    break
            elif self.m_Stage == 4:
                copysize = 0
                if self.m_Code2 == 0:
                    copysize = 3
                else:
                    sz = self.m_Code2 + 1
                    if self.has_bits(sz):
                        copysize = self.get_packed_bits(sz) + (1 << sz)
                    else:
                        break
                unpacked_chunk = self.copy(self.m_Shift, copysize, unpacked_chunk)
                self.m_Stage = 0
                continue
        return unpacked_chunk