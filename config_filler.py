#!/usr/bin/env python

# Python Perfect World Client,
# Config filler
# (c) GavYur, 2o13


import hashlib
import getpass
import sys
import os
import yaml


def text_input(text, required=False, default=None, func=raw_input):
    if default == None:
        text = '%s: ' % text
    elif default:
        text = '%s [%s]: ' % (text, default)
    else:
        text = '%s [\'\']: ' % text
    result = func(text).decode(sys.stdin.encoding)
    if required:
        while not result.strip():
            result = func(text).decode(sys.stdin.encoding)
    if result:
        return result
    else:
        return default


def digit_input(text, required=False, default=None, func=raw_input):
    if default == None:
        text = '%s: ' % text
    elif default:
        text = '%s [%s]: ' % (text, default)
    else:
        text = '%s [\'\']: ' % text
    result = func(text).decode(sys.stdin.encoding)
    if required:
        while not result.strip() and not result.isdigit():
            result = func(text).decode(sys.stdin.encoding)
    while (not result.isdigit() and result):
        result = func(text).decode(sys.stdin.encoding)
    if result:
        return int(result)
    else:
        return default


def bool_input(text, required=False, default=None, func=raw_input):
    if default == None:
        default_text = 'y/n'
    elif default:
        default_text = 'Y/n'
    else:
        default_text = 'y/N'
    text = '%s [%s]: ' % (text, default_text)
    result = func(text).decode(sys.stdin.encoding)
    search = ['y', 'n']
    if not required:
        search.append('')
    while not result.strip().lower() in search:
        result = func(text).decode(sys.stdin.encoding)
    if result:
        if result.lower() == 'y':
            return True
        elif result.lower() == 'n':
            return False
    else:
        return default


def main():
    os.chdir(os.path.abspath(sys.path[0]))

    login = text_input('Your account login', True)
    password = text_input('Your account password', True, func=getpass.getpass)
    write_pass = bool_input('Write password to config file?', default=False)
    character_name = text_input('Your character name', True)
    server = text_input('Server name or address to connect', True)
    force = bool_input('Force connect?', default=False)
    bankpassword = text_input('Your bank password (if set)', default='', func=getpass.getpass)
    meditation_type = text_input('Meditation type (standart, extended, or empty string)', default='')
    meditation_downgrade = bool_input('Meditation downgrade?', default=True)
    autoreply = text_input('Autoreply text', default='')
    syslogs = text_input('Syslogs directory', default='')
    chatlogs = text_input('Chatlogs directory', default='')
    keepalive = digit_input('Keepalive timeout', default=15)
    config_name = text_input('Config file address', default='./configs/config.yaml')

    config = {}
    config['login'] = login
    if write_pass:
        config['password'] = password
    else:
        config['logpass'] = hashlib.md5(login.lower() + password).hexdigest()
    config['character_name'] = character_name
    if server.count(':'):
        server = server.split(':', 1)
    config['server'] = server
    config['force'] = force
    config['bank_password'] = bankpassword
    config['meditation_type'] = meditation_type
    config['meditation_downgrade'] = meditation_downgrade
    config['autoreply'] = autoreply
    config['syslogs'] = syslogs
    config['chatlogs'] = chatlogs
    config['keepalive_timeout'] = keepalive
    f = file(config_name, 'w')
    yaml.safe_dump(config, f, default_flow_style=False, allow_unicode=True)


if __name__ == '__main__':
    try:
        main()
    except:
        pass