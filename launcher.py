#!/usr/bin/env python

# Python Perfect World Client,
# Launcher
# (c) GavYur, 2o13


from modules.printer import sprint
import os
import sys


def run_core(config_name):
    from modules import core
    core.main(config_name)


def main(argv):
    os.chdir(os.path.abspath(sys.path[0]))
    if argv:
        config_name = argv[0]
    else:
        config_name = os.path.join('configs', 'config.yaml')
    if os.path.isfile(config_name):
        run_core(config_name)
    else:
        sprint('Can\'t find config file (%s)' % config_name)


if __name__ == '__main__':
    main(sys.argv[1:])