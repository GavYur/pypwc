# Python Perfect World Client,
# server-to-client container packets description
# (c) GavYur, 2o13

from modules import cuint32, bytes
from packets import InContainerPacket

class PlayerList(InContainerPacket):
    def __init__(self, raw):
        super(PlayerList, self).__init__(rids[PlayerList], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        count = bytes.to_int(bytes.read_n(data, 2, True))
        self.players = {}
        while count:
            buid = bytes.read_n(data, 4, True)
            uid = bytes.to_int(buid)
            self.players[uid] = {}
            self.players[uid]['X'] = bytes.to_float(bytes.read_n(data, 4, True))
            self.players[uid]['Z'] = bytes.to_float(bytes.read_n(data, 4, True))
            self.players[uid]['Y'] = bytes.to_float(bytes.read_n(data, 4, True))
            bytes.read_n(data, 2)
            bytes.read_n(data, 2)
            self.players[uid]['angle'] = bytes.read_n(data, 1)
            bytes.read_n(data, 1)
            self.players[uid]['guildid'] = None
            self.players[uid]['get_equip'] = False
            self.players[uid]['short_info_uid'] = None
            mask = bytes.to_int(bytes.read_n(data, 4, True))
            if mask & 0x400:
                bytes.read_n(data, 8)
            if mask & 0x1:
                bytes.read_n(data, 1)
            if mask & 0x2:
                bytes.read_n(data, 1)
            if mask & 0x40:
                bytes.read_n(data, 16)
            if mask & 0x800:
                self.players[uid]['guildid'] = bytes.read_n(data, 4, True)
                self.players[uid]['guild_status'] = bytes.read_n(data, 1)
            if mask & 0x1000:
                bytes.read_n(data, 1)
                self.players[uid]['get_equip'] = True
            if mask & 0x10000:
                length = bytes.read_n(data, 1)
                bytes.read_n(data, length)
            if mask & 0x8:
                bytes.read_n(data, 1)
            if mask & 0x80000:
                bytes.read_n(data, 6)
            if mask & 0x100000:
                bytes.read_n(data, 5)
            if mask & 0x800000:
                self.players[uid]['short_info_uid'] = bytes.read_n(data, 4, True)
            if mask & 0x20000000:
                bytes.read_n(data, 4)
            count -= 1


class PlayerEnteredRadius(InContainerPacket):
    def __init__(self, raw):
        super(PlayerEnteredRadius, self).__init__(rids[PlayerEnteredRadius], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        buid = bytes.read_n(data, 4, True)
        uid = bytes.to_int(buid)
        self.uid = uid
        self.X = bytes.to_float(bytes.read_n(data, 4, True))
        self.Z = bytes.to_float(bytes.read_n(data, 4, True))
        self.Y = bytes.to_float(bytes.read_n(data, 4, True))
        bytes.read_n(data, 2)
        bytes.read_n(data, 2)
        self.angle = bytes.read_n(data, 1)
        bytes.read_n(data, 1)
        self.guildid = None
        self.get_equip = False
        self.short_info_uid = None
        mask = bytes.to_int(bytes.read_n(data, 4, True))
        if mask & 0x400:
            bytes.read_n(data, 8)
        if mask & 0x1:
            bytes.read_n(data, 1)
        if mask & 0x2:
            bytes.read_n(data, 1)
        if mask & 0x40:
            bytes.read_n(data, 16)
        if mask & 0x800:
            self.guildid = bytes.read_n(data, 4, True)
            self.guild_status = bytes.read_n(data, 1)
        if mask & 0x1000:
            bytes.read_n(data, 1)
            self.get_equip = True
        if mask & 0x10000:
            length = bytes.read_n(data, 1)
            bytes.read_n(data, length)
        if mask & 0x8:
            bytes.read_n(data, 1)
        if mask & 0x80000:
            bytes.read_n(data, 6)
        if mask & 0x100000:
            bytes.read_n(data, 5)
        if mask & 0x800000:
            self.short_info_uid = bytes.read_n(data, 4, True)
        if mask & 0x20000000:
            bytes.read_n(data, 4)


class PlayerBreakRadius(InContainerPacket):
    def __init__(self, raw):
        super(PlayerBreakRadius, self).__init__(rids[PlayerBreakRadius], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        buid = bytes.read_n(data, 4, True)
        self.uid = bytes.to_int(buid)


class LocationInfo(InContainerPacket):
    def __init__(self, raw):
        super(LocationInfo, self).__init__(rids[LocationInfo], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.locid = bytes.read_n(data, 1)


class PlayerPosition(InContainerPacket):
    def __init__(self, raw):
        super(PlayerPosition, self).__init__(rids[PlayerPosition], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.exp = bytes.to_int(bytes.read_n(data, 4, True))
        self.spirit = bytes.to_int(bytes.read_n(data, 4, True))
        bytes.read_n(data, 4)
        self.X = bytes.to_float(bytes.read_n(data, 4, True))
        self.Z = bytes.to_float(bytes.read_n(data, 4, True))
        self.Y = bytes.to_float(bytes.read_n(data, 4, True))


class PlayerMainInfo(InContainerPacket):
    def __init__(self, raw):
        super(PlayerMainInfo, self).__init__(rids[PlayerMainInfo], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.level = bytes.to_int(bytes.read_n(data, 2, True))
        bytes.read_n(data, 2)
        self.hp = bytes.to_int(bytes.read_n(data, 4, True))
        self.maxhp = bytes.to_int(bytes.read_n(data, 4, True))
        self.mp = bytes.to_int(bytes.read_n(data, 4, True))
        self.maxmp = bytes.to_int(bytes.read_n(data, 4, True))
        self.experience = bytes.to_int(bytes.read_n(data, 4, True))
        self.spirit = bytes.to_int(bytes.read_n(data, 4, True))


class MeditationInfo(InContainerPacket):
    def __init__(self, raw):
        super(MeditationInfo, self).__init__(rids[MeditationInfo], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.available_secs = bytes.to_int(bytes.read_n(data, 4, True))
        self.times = {}
        times_cnt = bytes.to_int(bytes.read_n(data, 4, True))
        for x in xrange(times_cnt):
            time_id = bytes.read_n(data, 4, True)
            seconds = bytes.to_int(bytes.read_n(data, 4, True))
            bytes.read_n(data, 4)
            if time_id != bytearray([255, 255, 255, 255]):
                self.times[bytes.to_int(time_id)] = seconds


class MeditationStatus(InContainerPacket):
    def __init__(self, raw):
        super(MeditationStatus, self).__init__(rids[MeditationStatus], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.mtype = bytes.to_int(bytes.read_n(data, 4, True))
        self.status = bytes.read_n(data, 1)


class Inventory(InContainerPacket):
    def __init__(self, raw):
        super(Inventory, self).__init__(rids[Inventory], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.type = cuint32.byte_to_int(data)
        self.slots = cuint32.byte_to_int(data)
        datalen = bytes.to_int(bytes.read_n(data, 4, True))
        itemscnt = bytes.to_int(bytes.read_n(data, 4, True))
        self.inventory = {}
        for x in xrange(itemscnt):
            slot = bytes.to_int(bytes.read_n(data, 4, True))
            item = bytes.to_int(bytes.read_n(data, 4, True))
            bytes.read_n(data, 8)
            cnt = bytes.to_int(bytes.read_n(data, 4, True))
            bytes.read_n(data, 2)
            infolen = bytes.to_int(bytes.read_n(data, 2, True))
            info = bytes.read_n(data, infolen)
            self.inventory[slot] = {'item': item, 'count': cnt, 'info': info}


class EquipItem(InContainerPacket):
    def __init__(self, raw):
        super(EquipItem, self).__init__(rids[EquipItem], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.inventory_slot = bytes.read_n(data, 1)
        self.eqinventory_slot = bytes.read_n(data, 1)
        self.source_inventory = bytes.to_int(bytes.read_n(data, 4, True))
        self.recv_inventory = bytes.to_int(bytes.read_n(data, 4, True))


class BankPasswordState(InContainerPacket):
    def __init__(self, raw):
        super(BankPasswordState, self).__init__(rids[BankPasswordState], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.isset = bool(bytes.read_n(data, 1))


class ItemEnterWorld(InContainerPacket):
    def __init__(self, raw):
        super(ItemEnterWorld, self).__init__(rids[ItemEnterWorld], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.id = bytes.read_n(data, 4)
        self.item = bytes.to_int(bytes.read_n(data, 4, True))
        self.X = bytes.to_float(bytes.read_n(data, 4, True))
        self.Z = bytes.to_float(bytes.read_n(data, 4, True))
        self.Y = bytes.to_float(bytes.read_n(data, 4, True))


class DroppedItem(InContainerPacket):
    def __init__(self, raw):
        super(DroppedItem, self).__init__(rids[DroppedItem], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.inventory = bytes.read_n(data, 1)
        self.slot = bytes.read_n(data, 1)
        self.count = bytes.to_int(bytes.read_n(data, 4, True))
        self.item = bytes.to_int(bytes.read_n(data, 4, True))


class AcceptedItem(InContainerPacket):
    def __init__(self, raw):
        super(AcceptedItem, self).__init__(rids[AcceptedItem], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.item = bytes.to_int(bytes.read_n(data, 4, True))
        bytes.read_n(data, 4)
        self.count = bytes.to_int(bytes.read_n(data, 4, True))
        self.count_all = bytes.to_int(bytes.read_n(data, 4, True))
        self.inventory = bytes.read_n(data, 1)
        self.slot = bytes.read_n(data, 1)


class PickedUpItem(AcceptedItem): pass


class Money(InContainerPacket):
    def __init__(self, raw):
        super(Money, self).__init__(rids[Money], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.money = bytes.to_int(bytes.read_n(data, 4, True))
        self.maxmoney = bytes.to_int(bytes.read_n(data, 4, True))


class SpendMoney(InContainerPacket):
    def __init__(self, raw):
        super(SpendMoney, self).__init__(rids[SpendMoney], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.money = bytes.to_int(bytes.read_n(data, 4, True))


class PickedUpMoney(InContainerPacket):
    def __init__(self, raw):
        super(PickedUpMoney, self).__init__(rids[PickedUpMoney], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.money = bytes.to_int(bytes.read_n(data, 4, True))


class ItemToMoney(InContainerPacket):
    def __init__(self, raw):
        super(ItemToMoney, self).__init__(rids[ItemToMoney], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.slot = bytes.to_int(bytes.read_n(data, 2, True))
        self.item = bytes.to_int(bytes.read_n(data, 4, True))
        self.count = bytes.to_int(bytes.read_n(data, 4, True))
        self.money = bytes.to_int(bytes.read_n(data, 4, True))


ids = {0x04: PlayerList,
       0x08: PlayerPosition,
       0x0c: PlayerEnteredRadius,
       0x0d: PlayerBreakRadius,
       0x12: ItemEnterWorld,
       0x1e: PickedUpMoney,
       0x1f: PickedUpItem,
       0x26: PlayerMainInfo,
       0x2b: Inventory,
       0x2e: DroppedItem,
       0x30: EquipItem,
       0x49: ItemToMoney,
       0x4d: SpendMoney,
       0x52: Money,
       0x81: BankPasswordState,
       0x9c: AcceptedItem,
       0xce: LocationInfo,
       0x149: MeditationInfo,
       0x14a: MeditationStatus}

rids = dict((ids[k], k) for k in ids)