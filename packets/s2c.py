# Python Perfect World Client,
# server-to-client packets description
# (c) GavYur, 2o13

from modules import cuint32, bytes
from packets import Packet, InContainerPacket

class ServerInfo(Packet):
    def __init__(self, raw):
        super(ServerInfo, self).__init__(rids[ServerInfo], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        keylen = bytes.read_n(data, 1)
        self.key = bytes.read_n(data, keylen)
        self.version = bytes.read_n(data, 4)
        self.strversion = '%d.%d.%d' % (self.version[1], self.version[2], self.version[3])
        self.auth_type = bytes.read_n(data, 1)
        crclen = bytes.read_n(data, 1)
        self.crc = bytes.read_n(data, crclen)
        self.exp_multiplier = bytes.read_n(data, 1)


class SMKey(Packet):
    def __init__(self, raw):
        super(SMKey, self).__init__(rids[SMKey], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        keylen = bytes.read_n(data, 1)
        self.key = bytes.read_n(data, keylen)
        self.force = bytes.read_n(data, 1)


class OnlineAnnounce(Packet):
    def __init__(self, raw):
        super(OnlineAnnounce, self).__init__(rids[OnlineAnnounce], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.key = bytes.read_n(data, 4)


class LastLogin(Packet):
    def __init__(self, raw):
        super(LastLogin, self).__init__(rids[LastLogin], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        acc_key = bytes.read_n(data, 4)
        bytes.read_n(data, 4)
        self.time = bytes.to_int(bytes.read_n(data, 4))
        self.last_ip = '.'.join(map(str, bytes.read_n(data, 4, True)))
        self.current_ip = '.'.join(map(str, bytes.read_n(data, 4, True)))


class RoleListRe(Packet):
    def __init__(self, raw):
        super(RoleListRe, self).__init__(rids[RoleListRe], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        bytes.read_n(data, 4)
        self.next_slotid = bytes.read_n(data, 4)
        self.account_key = bytes.read_n(data, 4)
        bytes.read_n(data, 4)
        self.ischar = bytes.read_n(data, 1)
        if self.ischar:
            self.charid = bytes.read_n(data, 4)
            self.gender = bytes.read_n(data, 1)
            self.race = bytes.read_n(data, 1)
            self.pclass = bytes.read_n(data, 1)
            self.level = bytes.to_int(bytes.read_n(data, 4))
            bytes.read_n(data, 4)
            namelen = bytes.read_n(data, 1)
            self.name = bytes.to_unicode(bytes.read_n(data, namelen))


class SelectRoleRe(Packet):
    pass


class ErrorInfo(Packet):
    def __init__(self, raw):
        super(ErrorInfo, self).__init__(rids[ErrorInfo], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        self.message = str(raw).strip()


class PacketsContainer(Packet):
    def __init__(self, raw):
        super(PacketsContainer, self).__init__(rids[PacketsContainer], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        self.packets = []
        new_packet = Packet(raw=raw)
        surplus = new_packet.get_surplus()
        self.packets.append(InContainerPacket(raw=new_packet.buffer))
        while surplus:
            new_packet = Packet(raw=surplus)
            surplus = new_packet.get_surplus()
            self.packets.append(InContainerPacket(raw=new_packet.buffer))


class WorldMessage(Packet):
    def __init__(self, raw):
        super(WorldMessage, self).__init__(rids[WorldMessage], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.type = bytes.read_n(data, 1)
        bytes.read_n(data, 1)
        self.uid = bytes.read_n(data, 4)
        nicklen = cuint32.byte_to_int(data)
        self.nick = bytes.to_unicode(bytes.read_n(data, nicklen))
        textlen = cuint32.byte_to_int(data)
        self.text = bytes.to_unicode(bytes.read_n(data, textlen))


class ChatMessage(Packet):
    def __init__(self, raw):
        super(ChatMessage, self).__init__(rids[ChatMessage], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.type = bytes.read_n(data, 1)
        bytes.read_n(data, 1)
        self.uid = bytes.read_n(data, 4)
        textlen = cuint32.byte_to_int(data)
        self.text = bytes.to_unicode(bytes.read_n(data, textlen))


class PrivateMessage(Packet):
    def __init__(self, raw):
        super(PrivateMessage, self).__init__(rids[PrivateMessage], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.type = bytes.read_n(data, 1)
        self.flag = bytes.read_n(data, 1)
        nicklen = cuint32.byte_to_int(data)
        self.nick = bytes.to_unicode(bytes.read_n(data, nicklen))
        self.uid = bytes.read_n(data, 4)
        nick2len = cuint32.byte_to_int(data)
        bytes.read_n(data, nick2len)
        bytes.read_n(data, 4)
        textlen = cuint32.byte_to_int(data)
        self.text = bytes.to_unicode(bytes.read_n(data, textlen))
        bytes.read_n(data, 1)
        self.level = bytes.to_int(bytes.read_n(data, 4))


class GuildMessage(Packet):
    def __init__(self, raw):
        super(GuildMessage, self).__init__(rids[GuildMessage], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        self.type = bytes.read_n(data, 1)
        bytes.read_n(data, 1)
        self.uid = bytes.read_n(data, 4)
        textlen = cuint32.byte_to_int(data)
        self.text = bytes.to_unicode(bytes.read_n(data, textlen))


class PlayerAppearance(Packet):
    def __init__(self, raw):
        super(PlayerAppearance, self).__init__(rids[PlayerAppearance], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        bytes.read_n(data, 13)
        buid = bytes.read_n(data, 4)
        self.uid = bytes.to_int(buid)
        nicklen = bytes.read_n(data, 1)
        self.nick = bytes.to_unicode(bytes.read_n(data, nicklen))
        bytes.read_n(data, 4)
        self.race = bytes.read_n(data, 4)
        self.gender = bytes.read_n(data, 1)


class PlayerShortInfo(Packet):
    def __init__(self, raw):
        super(PlayerShortInfo, self).__init__(rids[PlayerShortInfo], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        bytes.read_n(data, 13)
        buid = bytes.read_n(data, 4)
        self.uid = bytes.to_int(buid)
        bytes.read_n(data, 1)
        nicklen = bytes.read_n(data, 1)
        self.nick = bytes.to_unicode(bytes.read_n(data, nicklen))


class FriendList(Packet):
    def __init__(self, raw):
        super(FriendList, self).__init__(rids[FriendList], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        bytes.read_n(data, 4)
        self.friends = {}
        friends_uids = []
        groups = ['default']
        groupscnt = cuint32.byte_to_int(data)
        for x in xrange(groupscnt):
            groupid = cuint32.byte_to_int(data)
            namelen = cuint32.byte_to_int(data)
            name = bytes.to_unicode(bytes.read_n(data, namelen))
            groups.insert(groupid, name)
        count = cuint32.byte_to_int(data)
        for x in xrange(count):
            uid = bytes.to_int(bytes.read_n(data, 4))
            race = bytes.read_n(data, 1)
            groupid = bytes.read_n(data, 1)
            nicklen = cuint32.byte_to_int(data)
            nick = bytes.to_unicode(bytes.read_n(data, nicklen))
            friends_uids.append(uid)
            self.friends[uid] = {'nick': nick, 'race': race, 'online': 0, 'group': groups[groupid]}
        flagscnt = cuint32.byte_to_int(data)
        flags = bytes.read_n(data, flagscnt)
        for flag in zip(flags[::2], flags[1::2]):
            if flag[1]:
                self.friends[friends_uids[flag[0]]]['online'] = 1


class ACRemoteCode(Packet):
    def __init__(self, raw):
        super(ACRemoteCode, self).__init__(rids[ACRemoteCode], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        # simply get two-bytes for reply
        raw.reverse()
        data = iter(raw)
        bytes.read_n(data, 2)
        self.necessary_bytes = bytes.read_n(data, 2)


class SavedMessages(Packet):
    def __init__(self, raw):
        super(SavedMessages, self).__init__(rids[SavedMessages], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        count = bytes.to_int(bytes.read_n(data, 2))
        self.messages = []
        for x in range(count):
            message = {}
            message['type'] = bytes.read_n(data, 1)
            nicklen = cuint32.byte_to_int(data)
            message['nick'] = bytes.to_unicode(bytes.read_n(data, nicklen))
            message['uid'] = bytes.read_n(data, 4)
            tonicklen = cuint32.byte_to_int(data)
            message['tonick'] = bytes.to_unicode(bytes.read_n(data, tonicklen))
            message['touid'] = bytes.read_n(data, 4)
            msglen = cuint32.byte_to_int(data)
            message['text'] = bytes.to_unicode(bytes.read_n(data, msglen))
            self.messages.append(message)
        self.uid = bytes.read_n(data, 4)


class FriendStatus(Packet):
    def __init__(self, raw):
        super(FriendStatus, self).__init__(rids[FriendStatus], raw)
        self.parse_raw(raw)

    def parse_raw(self, raw):
        data = iter(raw)
        buid = bytes.read_n(data, 4)
        self.uid = bytes.to_int(buid)
        self.online = bytes.read_n(data, 1)


ids = {0x00: PacketsContainer,
       0x01: ServerInfo,
       0x02: SMKey,
       0x04: OnlineAnnounce,
       0x05: ErrorInfo,
       0x47: SelectRoleRe,
       0x50: ChatMessage,
       0x53: RoleListRe,
       0x5c: PlayerAppearance,
       0x60: PrivateMessage,
       0x6c: PlayerShortInfo,
       0x85: WorldMessage,
       0x8f: LastLogin,
       0xcf: FriendList,
       0xda: SavedMessages,
       0xd6: FriendStatus,
       0x12c3: GuildMessage,
       0x138b: ACRemoteCode}

rids = dict((ids[k], k) for k in ids)