# Python Perfect World Client,
# Packets description
# (c) GavYur, 2o13

from modules import cuint32, bytes

class Packet(object):
    def __init__(self, ptype=0, data=bytearray(), raw=bytearray()):
        if raw:
            self.parse_raw(raw)
        else:
            self.type = ptype
            self.buffer = data
            self.surplus = bytearray()
            self.not_enough = 0

    def __str__(self):
        return str(self.get_bytearray())

    def get_bytearray(self):
        result = bytearray()
        result += cuint32.int_to_byte(self.type)
        result += cuint32.int_to_byte(len(self.buffer))
        result += self.buffer
        return result

    def get_surplus(self):
        res = self.surplus
        self.clear_surplus()
        return res

    def clear_surplus(self):
        self.surplus = bytearray()

    def add_data(self, data):
        if len(data) == self.not_enough:
            self.buffer += data
            self.not_enough = 0
        elif len(data) > self.not_enough:
            self.buffer += data[:self.not_enough]
            del data[:self.not_enough]
            self.surplus = data
            self.not_enough = 0
        else:
            self.buffer += data
            self.not_enough -= len(data)

    def parse_raw(self, raw):
        raw = iter(raw)
        self.type = cuint32.byte_to_int(raw)
        length = cuint32.byte_to_int(raw)
        raw = bytearray(raw)
        if len(raw) == length:
            self.buffer = raw
            self.surplus = bytearray()
            self.not_enough = 0
        elif len(raw) > length:
            self.buffer = raw[:length]
            del raw[:length]
            self.surplus = raw
            self.not_enough = 0
        else:
            self.buffer = raw
            self.surplus = bytearray()
            self.not_enough = length - len(raw)

class InContainerPacket(Packet):
    def get_bytearray(self):
        result = bytearray()
        result += cuint32.int_to_byte(len(self.buffer) + 2)
        result += bytes.from_int(self.type, 2, True)
        result += self.buffer
        return result

    def parse_raw(self, raw):
        raw = iter(raw)
        length = cuint32.byte_to_int(raw)
        self.type = bytes.to_int(bytearray([raw.next(), raw.next()]), True)
        raw = bytearray(raw)
        if len(raw) == length:
            self.buffer = raw
            self.surplus = bytearray()
            self.not_enough = 0
        elif len(raw) > length:
            self.buffer = raw[:length]
            del raw[:length]
            self.surplus = raw
            self.not_enough = 0
        else:
            self.buffer = raw
            self.surplus = bytearray()
            self.not_enough = length - len(raw)

import c2s
import cc2s
import s2c
import sc2c