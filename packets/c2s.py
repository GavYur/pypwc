# Python Perfect World Client,
# client-to-server packets description
# (c) GavYur, 2o13

from modules import cuint32, bytes
from packets import Packet, InContainerPacket

class LogginAnnounce(Packet):
    def __init__(self, login, hash):
        self.login = login
        self.hash = hash
        data = self.get_buffer()
        ptype = rids[LogginAnnounce]
        super(LogginAnnounce, self).__init__(ptype, data)

    def get_buffer(self):
        loginlen = len(self.login)
        hashlen = len(self.hash)
        result = bytearray()
        result.append(loginlen)
        result += bytearray(self.login)
        result.append(hashlen)
        result += bytearray(self.hash)
        result += bytearray([0, 4, 255, 255, 255, 255])
        return result


class CMKey(Packet):
    def __init__(self, key, force):
        self.key = key
        self.force = force
        data = self.get_buffer()
        ptype = rids[CMKey]
        super(CMKey, self).__init__(ptype, data)

    def get_buffer(self):
        keylen = len(self.key)
        result = bytearray()
        result.append(keylen)
        result += self.key
        result.append(self.force)
        return result


class RoleList(Packet):
    def __init__(self, key, slot):
        self.key = key
        self.slot = slot
        data = self.get_buffer()
        ptype = rids[RoleList]
        super(RoleList, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += self.key
        result += bytearray([0, 0, 0, 0])
        result += self.slot
        return result


class SelectRole(Packet):
    def __init__(self, uid):
        self.uid = uid
        data = self.get_buffer()
        ptype = rids[SelectRole]
        super(SelectRole, self).__init__(ptype, data)

    def get_buffer(self):
        return self.uid


class EnterWorld(Packet):
    def __init__(self, uid):
        self.uid = uid
        data = self.get_buffer()
        ptype = rids[EnterWorld]
        super(EnterWorld, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += self.uid
        result += bytearray([0, 0, 0, 0] * 5)
        return result


class KeepAlive(Packet):
    def __init__(self):
        data = self.get_buffer()
        ptype = rids[KeepAlive]
        super(KeepAlive, self).__init__(ptype, data)

    def get_buffer(self):
        return bytearray([0x5a])


class GetPlayersAppearance(Packet):
    def __init__(self, myuid, uids):
        self.myuid = myuid
        self.uids = uids
        data = self.get_buffer()
        ptype = rids[GetPlayersAppearance]
        super(GetPlayersAppearance, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += self.myuid
        result += bytearray([0, 0, 0, 0])
        result += cuint32.int_to_byte(len(self.uids))
        for uid in self.uids:
            result += uid
        return result


class GetGuildsInfo(Packet):
    def __init__(self, myuid, guildids):
        self.myuid = myuid
        self.guildids = guildids
        data = self.get_buffer()
        ptype = rids[GetGuildsInfo]
        super(GetGuildsInfo, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += self.myuid
        result += bytearray([0, 0, 0, 0])
        result += cuint32.int_to_byte(len(self.guildids))
        for guildid in self.guildids:
            result += guildid
        return result


class GetPlayerShortInfo(Packet):
    def __init__(self, myuid, uid, gtype):
        self.myuid = myuid
        self.uid = uid
        self.gtype = gtype
        data = self.get_buffer()
        ptype = rids[GetPlayerShortInfo]
        super(GetPlayerShortInfo, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += self.myuid
        result += bytearray([0, 0, 0, 0])
        result.append(1)
        result += self.uid
        result.append(self.gtype)
        return result


class PacketsContainer(Packet):
    def __init__(self, packets):
        self.packets = packets
        data = self.get_buffer()
        ptype = rids[PacketsContainer]
        super(PacketsContainer, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        for packet in self.packets:
            result += packet.get_bytearray()
        return result


class GetFriendList(Packet):
    def __init__(self, myuid):
        self.myuid = myuid
        data = self.get_buffer()
        ptype = rids[GetFriendList]
        super(GetFriendList, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += self.myuid
        result += bytearray([0, 0, 0, 0])
        return result


class SendPrivateMessage(Packet):
    def __init__(self, mtype, flag, myuid, mynick, tonick, touid, message, end):
        self.mtype = mtype
        self.flag = flag
        self.myuid = myuid
        self.mynick = mynick
        self.tonick = tonick
        self.touid = touid
        self.message = message
        self.end = end
        data = self.get_buffer()
        ptype = rids[SendPrivateMessage]
        super(SendPrivateMessage, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result.append(self.mtype)
        result.append(self.flag)
        result += cuint32.int_to_byte(len(self.mynick) * 2)
        result += bytes.from_unicode(self.mynick, True)
        result += self.myuid
        result += cuint32.int_to_byte(len(self.tonick) * 2)
        result += bytes.from_unicode(self.tonick, True)
        result += self.touid
        result += cuint32.int_to_byte(len(self.message) * 2)
        result += bytes.from_unicode(self.message, True)
        result.append(0)
        result += self.end
        return result


class SendChatMessage(Packet):
    def __init__(self, myuid, message):
        self.myuid = myuid
        self.message = message
        data = self.get_buffer()
        ptype = rids[SendChatMessage]
        super(SendChatMessage, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += bytearray([0, 223])
        result += self.myuid
        result += bytearray([0, 24, 233, 172])
        result += cuint32.int_to_byte(len(self.message) * 2)
        result += bytes.from_unicode(self.message, True)
        result.append(0)
        return result


class SendGuildMessage(Packet):
    def __init__(self, myuid, message):
        self.myuid = myuid
        self.message = message
        data = self.get_buffer()
        ptype = rids[SendGuildMessage]
        super(SendGuildMessage, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += bytearray([0, 0])
        result += self.myuid
        result += cuint32.int_to_byte(len(self.message) * 2)
        result += bytes.from_unicode(self.message, True)
        result += bytearray([0, 0, 0, 0, 0])
        return result


class ACReport(Packet):
    def __init__(self, myuid, two_bytes):
        self.myuid = myuid
        self.two_bytes = two_bytes
        data = self.get_buffer()
        ptype = rids[ACReport]
        super(ACReport, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += self.myuid
        result += bytearray([10, 8, 8, 10, 1])
        result += self.two_bytes
        result += bytearray([0, 0, 212, 49])
        return result


class GetSavedMessages(Packet):
    def __init__(self, myuid):
        self.myuid = myuid
        data = self.get_buffer()
        ptype = rids[GetSavedMessages]
        super(GetSavedMessages, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += self.myuid
        result += bytearray([0, 0, 0, 0])
        return result


ids = {0x02: CMKey,
       0x03: LogginAnnounce,
       0x22: PacketsContainer,
       0x46: SelectRole,
       0x48: EnterWorld,
       0x4f: SendChatMessage,
       0x52: RoleList,
       0x5a: KeepAlive,
       0x5b: GetPlayersAppearance,
       0x60: SendPrivateMessage,
       0x6b: GetPlayerShortInfo,
       0xce: GetFriendList,
       0xd9: GetSavedMessages,
       0x12c3: SendGuildMessage,
       0x12ce: GetGuildsInfo,
       0x1389: ACReport}

rids = dict((ids[k], k) for k in ids)