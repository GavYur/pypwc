# Python Perfect World Client,
# client-to-server container packets description
# (c) GavYur, 2o13

from modules import bytes
from packets import InContainerPacket

class GetPlayersEquip(InContainerPacket):
    def __init__(self, players):
        self.players = players
        data = self.get_buffer()
        ptype = rids[GetPlayersEquip]
        super(GetPlayersEquip, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += bytes.from_int(len(self.players), 2, True)
        for player in self.players:
            player.reverse()
            result += player
        return result


class GetSelfInfo(InContainerPacket):
    def __init__(self, flags):
        self.flags = flags
        data = self.get_buffer()
        ptype = rids[GetSelfInfo]
        super(GetSelfInfo, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        for flag in self.flags:
            result.append(flag)
        return result


class SwitchMeditation(InContainerPacket):
    def __init__(self, mtype, on):
        self.mtype = mtype
        self.on = on
        data = self.get_buffer()
        ptype = rids[SwitchMeditation]
        super(SwitchMeditation, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += bytes.from_int(self.mtype, 4, True)
        result.append(self.on)
        return result


class EquipItem(InContainerPacket):
    def __init__(self, inv_slot, eqinv_slot):
        self.inv_slot = inv_slot
        self.eqinv_slot = eqinv_slot
        data = self.get_buffer()
        ptype = rids[EquipItem]
        super(EquipItem, self).__init__(ptype, data)

    def get_buffer(self):
        return bytearray([self.inv_slot, self.eqinv_slot])


class BankPassword(InContainerPacket):
    def __init__(self, password=''):
        self.password = password
        data = self.get_buffer()
        ptype = rids[BankPassword]
        super(BankPassword, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += bytes.from_int(len(self.password), 4, True)
        result += self.password
        return result


class IncreaseMeditationTime(InContainerPacket):
    def __init__(self, mtype, item, slot, cnt):
        self.mtype = mtype
        self.item = item
        self.slot = slot
        self.count = cnt
        data = self.get_buffer()
        ptype = rids[IncreaseMeditationTime]
        super(IncreaseMeditationTime, self).__init__(ptype, data)

    def get_buffer(self):
        result = bytearray()
        result += bytes.from_int(self.mtype, 4, True)
        result += bytearray([1, 0, 0, 0])
        result += bytes.from_int(self.item, 4, True)
        result += bytes.from_int(self.slot, 4, True)
        result += bytes.from_int(self.count, 4, True)
        return result


ids = {0x11: EquipItem,
       0x21: GetPlayersEquip,
       0x27: GetSelfInfo,
       0x78: BankPassword,
       0x85: IncreaseMeditationTime,
       0x86: SwitchMeditation}

rids = dict((ids[k], k) for k in ids)